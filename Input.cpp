//
// Created by alexey on 27.04.19.
//

#include "Input.h"

void Input::handle_player_input(sf::Keyboard::Key key, bool is_pressed) {
    if (key == sf::Keyboard::W)
        m_is_w = is_pressed;
    else if (key == sf::Keyboard::S)
        m_is_s = is_pressed;
    else if (key == sf::Keyboard::A)
        m_is_a = is_pressed;
    else if (key == sf::Keyboard::D)
        m_is_d = is_pressed;
    else if (key == sf::Keyboard::E)
        m_is_e = is_pressed;
    else if (key == sf::Keyboard::F)
        m_is_f = is_pressed;
    else if (key == sf::Keyboard::Space)
        m_is_space = is_pressed;
}
