//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_PLAYER_H
#define ZEPPELIN_PLAYER_H


#include "Entity.h"
#include "Health.h"
#include "Stamina.h"
#include "Mana.h"

class Player : public Entity {
public:
    Player(unsigned int h, unsigned int m, unsigned int s) : Entity() {
        health.set_health(h);
        mana.set_mana(m);
        stamina.set_stamina(s);
    };

    virtual void relationship(Entity&, Entity&);

    Health health;
    Stamina stamina;
    Mana mana;
private:

};


#endif //ZEPPELIN_PLAYER_H
