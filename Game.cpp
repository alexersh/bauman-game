//
// Created by alexey on 27.04.19.
//

#include "Game.h"
#include <SFML/Graphics.hpp>

void Game::run() {
    sf::Clock clock;
    sf::Time time_scince_last_update = sf::Time::Zero;
    while (m_window.isOpen()) {
        processEvents();
        time_scince_last_update += clock.restart();
        while (time_scince_last_update > time_per_frame) {
            time_scince_last_update -= time_per_frame;
            update();
        }
        render();
    }
}

//TODO
void Game::set_resolution(unsigned int width, unsigned int length) {

}

//TODO
Game::Game() : m_window(sf::VideoMode(640, 459), "Zeppelin") {
    background.set_texture_back("../Media/Textures/Background/back.jpg");
    background.set_sprite_back();
}

void Game::processEvents() {
    sf::Event event;
    Input input;
    while (m_window.pollEvent(event)) {
        switch (event.type) {
            case sf::Event::Closed :
                m_window.close();
                break;
            case sf::Event::KeyPressed:
                input.handle_player_input(event.key.code, true);
                break;
            case sf::Event::KeyReleased:
                input.handle_player_input(event.key.code, false);
                break;
        }
    }
}

void Game::update() {

}

void Game::render() {
    m_window.clear();
    background.draw(m_window);
    m_window.display();
}
