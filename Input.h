//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_INPUT_H
#define ZEPPELIN_INPUT_H


#include <SFML/Window.hpp>

class Input {
public:
    Input() = default;

    void handle_player_input(sf::Keyboard::Key, bool is_pressed);

    bool get_is_w() { return m_is_w; }

    bool get_is_s() { return m_is_s; }

    bool get_is_a() { return m_is_a; }

    bool get_is_d() { return m_is_d; }

    bool get_is_e() { return m_is_e; }

    bool get_is_f() { return m_is_f; }

    bool get_is_space() { return m_is_space; }

private:

    bool m_is_w = false;
    bool m_is_s = false;
    bool m_is_a = false;
    bool m_is_d = false;
    bool m_is_space = false;
    bool m_is_e = false;
    bool m_is_f = false;
};


#endif //ZEPPELIN_INPUT_H
