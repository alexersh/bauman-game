//
// Created by alexey on 27.04.19.
//

#include <iostream>
#include "Entity.h"

void Entity::set_texture(std::string path) {
    if (!m_texture.loadFromFile(path)) {
        std::cout << "Usage: fail to load texture " << path << std::endl;
        exit(-1);
    }
}

void Entity::set_sprite() {
    m_sprite.setTexture(m_texture);
}
