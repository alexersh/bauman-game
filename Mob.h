//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_MOB_H
#define ZEPPELIN_MOB_H


#include "Entity.h"
#include "Health.h"

class Mob : public Entity {
public:
    explicit Mob(unsigned int h) : Entity() {
        health.set_health(h);
    };

    virtual void relationship(Entity&, Entity&);

    Health health;
private:
};


#endif //ZEPPELIN_MOB_H
