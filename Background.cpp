//
// Created by alexey on 27.04.19.
//

#include <iostream>
#include "Background.h"
#include <SFML/Graphics.hpp>

void Background::set_texture_back(std::string path) {
    if (!m_back_texture.loadFromFile(path)) {
        std::cout << "Usage: fail to load texture" << path << std::endl;
        exit(-1);
    }
}

void Background::set_sprite_back() {
    m_back.setTexture(m_back_texture);
    m_back.setPosition(0.f, 0.f);
}

void Background::draw(sf::RenderWindow &window) {
    window.draw(m_back);
}


