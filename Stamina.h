//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_STAMINA_H
#define ZEPPELIN_STAMINA_H


class Stamina {
public:
    Stamina() = default;

    void set_stamina(unsigned int buffer) { m_stamina = buffer; }

    unsigned int get_stamina() { return m_stamina; }

private:
    unsigned int m_stamina = 1;
};


#endif //ZEPPELIN_STAMINA_H
