//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_ENTITY_H
#define ZEPPELIN_ENTITY_H


#include <SFML/Graphics.hpp>

class Entity {
public:

    Entity() = default;

 //  virtual ~Entity() = 0;

    void set_texture(std::string path);

    void set_sprite();

    sf::Sprite get_sprite() { return m_sprite; }
    virtual void relationship(Entity&, Entity&) = 0;
protected:
    sf::Texture m_texture;
    sf::Sprite m_sprite;
};


#endif //ZEPPELIN_ENTITY_H
