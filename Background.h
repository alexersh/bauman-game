//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_BACKGROUND_H
#define ZEPPELIN_BACKGROUND_H



#include <string>
#include <SFML/Graphics.hpp>

class Background {
public:
    void set_texture_back(std::string path);

    void draw(sf::RenderWindow &window);

    void set_sprite_back();

private:
    sf::Texture m_back_texture;
    sf::Texture m_middle_texture;
    sf::Texture m_main_texture;
    sf::Texture m_front_texture;

    sf::Sprite m_back;
    sf::Sprite m_middle;
    sf::Sprite m_main;
    sf::Sprite m_front;
};


#endif //ZEPPELIN_BACKGROUND_H
