CMAKE_MINIMUM_REQUIRED(VERSION 3.10)
PROJECT(Zeppelin)

SET(CMAKE_CXX_STANDARD 17)
SET(EXECUTABLE_NAME Zeppelin)


ADD_EXECUTABLE(${EXECUTABLE_NAME} main.cpp Game.cpp Game.h Input.cpp Input.h Entity.cpp Entity.h Player.cpp Player.h Mob.cpp Mob.h Mob_Distance.cpp Mob_Distance.h Background.cpp Background.h Health.cpp Health.h Mana.cpp Mana.h Stamina.cpp Stamina.h)

SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMakeModules")

FIND_PACKAGE(SFML 2 REQUIRED network audio graphics window system)

if(SFML_FOUND)
    INCLUDE_DIRECTORIES(${SFML_INCLUDE_DIR})
    TARGET_LINK_LIBRARIES(${EXECUTABLE_NAME} ${SFML_LIBRARIES} ${SFML_DEPENDENCIES})
endif()