//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_HEALTH_H
#define ZEPPELIN_HEALTH_H


class Health {
public:
    Health() = default;

    unsigned int get_health() { return m_health; }

    void set_health(unsigned int buffer) { m_health = buffer; }

private:
    unsigned int m_health = 1;
};


#endif //ZEPPELIN_HEALTH_H
