//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_MOB_DISTANCE_H
#define ZEPPELIN_MOB_DISTANCE_H


#include "Entity.h"
#include "Health.h"

class Mob_Distance : public Entity {
public:
    explicit Mob_Distance(unsigned int h) : Entity() {
        health.set_health(h);
    };

    virtual void relationship(Entity&, Entity&);

    Health health;
private:
};


#endif //ZEPPELIN_MOB_DISTANCE_H
