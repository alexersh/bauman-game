//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_MANA_H
#define ZEPPELIN_MANA_H


class Mana {
public:
    Mana() = default;

    void set_mana(unsigned int buffer) { m_mana = buffer; }

    unsigned int get_mana() { return m_mana; }

private:
    unsigned int m_mana = 1;
};


#endif //ZEPPELIN_MANA_H
