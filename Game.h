//
// Created by alexey on 27.04.19.
//

#ifndef ZEPPELIN_GAME_H
#define ZEPPELIN_GAME_H


#include <SFML/Graphics.hpp>
#include "Input.h"
#include "Background.h"
#include "Player.h"
#include "Mob.h"
#include "Mob_Distance.h"

class Game {
public:
    Game();

    void run();

    void set_resolution(unsigned int width, unsigned int length);

private:
    Background background;
    Player player{10, 10, 10};
    Mob mob{10};
    Mob_Distance mob_distance{10};
    Input input;

    void processEvents();

    void update();

    void render();

    sf::RenderWindow m_window;
    sf::Time const time_per_frame = sf::seconds(1.f / 60.f);
};


#endif //ZEPPELIN_GAME_H
